import Chain from "../blockchain/Chain";

const DB = require('better-sqlite3');
const Promise = require("bluebird");

export default class Database {
    constructor(name) {
        const chainDataPath = './chaindata';
        const ext = 'chain';
        const defaultName = 'default';
        name = (name ? name : defaultName).toString().trim();
        if (!name) {
            name = `${defaultName}.${ext}`;
        } else {
            name = `${name}.${ext}`;
        }
        this.db = new DB(`${chainDataPath}/${name}`);
        this._createTable();
        this._initStmts();
    }

    /**
     * @description 初始化指令
     * @private
     */
    _initStmts() {
        this.insertStmt = this.db.prepare('INSERT INTO block VALUES (@index, @timestamp, @blockData, @previousHash, @difficulty, @nonce, @hash)');
        this.getStmt = this.db.prepare('SELECT * FROM block WHERE blockIndex=@index');
        this.allStmt = this.db.prepare('SELECT * FROM block');
        this.delStmt = this.db.prepare('DELETE FROM block');
    }

    /**
     * @description  创建数据表
     * @returns {boolean}
     * @private
     */
    _createTable() {
        try {
            this.db.exec(`CREATE TABLE IF NOT EXISTS block (blockIndex integer PRIMARY KEY, timestamp text NOT NULL, blockData text NOT NULL, previousHash text NOT NULL, difficulty integer NOT NULL, nonce integer NOT NULL, hash text NOT NULL)`);
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    /**
     * @description 获取已储存的链数据
     * @return {Array}
     * @private
     */
    _all() {
        const chain = [];
        for (const _block of this.allStmt.iterate()) {
            chain.push({
                index: _block.blockIndex,
                timestamp: _block.timestamp,
                blockData: _block.blockData,
                previousHash: _block.previousHash,
                difficulty: _block.difficulty,
                nonce: _block.nonce,
                hash: _block.hash
            });
        }
        return chain;
    }

    /**
     * @description 插入区块数据至数据库
     * @param blockObj
     * @returns {boolean}
     * @private
     */
    _insert(blockObj) {
        try {
            const exist = this._get(blockObj.index);
            if (!exist) {
                const res = this.insertStmt.run(blockObj);
                return !!res.changes;
            } else {
                return false;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    /**
     * @description 获取区块数据
     * @param {int} index
     * @returns {*}
     * @private
     */
    _get(index) {
        try {
            const res = this.getStmt.get({index: index});
            if (!res) return null;
            return {
                index: res.blockIndex,
                timestamp: res.timestamp,
                blockData: res.blockData,
                previousHash: res.previousHash,
                difficulty: res.difficulty,
                nonce: res.nonce,
                hash: res.hash
            };
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    /**
     * @description 将链条数据存储
     * @param {Chain} chain
     * @return {Promise}
     */
    save(chain) {
        return new Promise((resolve, reject) => {
            if (!chain.verify()) {
                reject(new Error('Fail to verify chain data'));
            } else {
                const blocks = chain.export();
                for (const block of blocks) {
                    if (!this._insert(block.toObject())) {
                        reject(new Error('Fail to save chain data to database'));
                        return;
                    }
                }
                resolve(chain);
            }
        });
    }

    /**
     * @description 还原为链
     * @return {Chain}
     */
    restore() {
        const chainData = this._all();
        return new Chain(chainData);
    }

    /**
     * @description 清空数据表
     * @return {*}
     */
    empty() {
        try {
            return this.delStmt.run();
        } catch (e) {
            console.error(e);
            return false;
        }
    }
}