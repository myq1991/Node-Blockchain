import {SHA256} from 'crypto-js';

export default class Block {
    /**
     * @param {int} index
     * @param {string} timestamp
     * @param {*} blockData
     * @param {string} previousHash
     * @param {int} difficulty
     * @param {int} [nonce]
     */
    constructor(index, timestamp, blockData, previousHash, difficulty, nonce = 0) {
        this.index = index;
        this.timestamp = timestamp;
        this.blockData = blockData;
        this.previousHash = previousHash;
        this.difficulty = difficulty;
        this.nonce = nonce;
        this.hash = this.calculateBlockHash();
    }

    /**
     * @description 获取被哈希数据对象
     * @returns {{index: int|*, timestamp: string|*, blockData: *, previousHash: string|*, difficulty: int|*, nonce: int|*}}
     */
    getHashObject() {
        return {
            index: this.index,
            timestamp: this.timestamp,
            blockData: this.blockData,
            previousHash: this.previousHash,
            difficulty: this.difficulty,
            nonce: this.nonce
        };
    }

    /**
     * @description 计算区块对象哈希值
     * @returns {string}
     */
    calculateBlockHash() {
        let hashObj = this.getHashObject();
        while (SHA256(JSON.stringify(hashObj)).toString().substring(0, this.difficulty) !== Array(this.difficulty + 1).join('0')) {
            this.nonce++;
            hashObj = this.getHashObject();
        }
        return SHA256(JSON.stringify(hashObj)).toString();
    }

    /**
     * @description 验证区块数据是否正确
     * @returns {boolean}
     */
    verify() {
        const hashObj = this.getHashObject();
        return this.hash === SHA256(JSON.stringify(hashObj)).toString();
    }

    /**
     * @description 导出为对象
     * @returns {{index: int|*, timestamp: string|*, blockData: *, previousHash: string|*, difficulty: int|*, nonce: int|*, hash: string|*}}
     */
    toObject() {
        return {
            index: this.index,
            timestamp: this.timestamp,
            blockData: this.blockData,
            previousHash: this.previousHash,
            difficulty: this.difficulty,
            nonce: this.nonce,
            hash: this.hash
        };
    }

    /**
     * @description 导出为JSON格式数据
     * @returns {string}
     */
    toJSON() {
        return JSON.stringify(this.toObject());
    }
}