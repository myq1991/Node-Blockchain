import Database from "../datastore/Database";

export default class Blockchain {
    constructor(name = 'default') {
        this.name = name ? name : 'default';
        this.db = new Database(this.name);
        this.sync();//todo 有待确认
    }

    appendBlock() {

    }

    /**
     * @description 区块链数据落地
     * @private
     */
    _save() {
        this.db.save(this.chain).then(chain => {
            if (!chain.verify()) {
                this.db.empty();
            }
        }).catch(err => {
            this._log(err);
        }).finally(() => {
            this.sync();
        });
    }

    /**
     * @description 记录日志
     * @param data
     * @private
     */
    _log(data) {
        const fs = require('fs');
        const logFile = `../../runtime/${Date.now()}.log`;
        fs.writeFileSync(logFile, JSON.stringify({
            data: data
        }));
    }

    sync() {
        //todo 同步区块链
        setTimeout(() => {
            this.chain = this.db.restore();
            this._save();
        }, 3000);
    }

    /**
     * @description 获取区块链的名称
     * @return {string|*}
     */
    getName() {
        return this.name;
    }
}