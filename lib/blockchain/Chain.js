import Block from "./Block";

export default class Chain {
    constructor(chainData = []) {
        this.chain = [];
        chainData = chainData || [];
        for (const jsonBlock of chainData) {
            const block = JSON.parse(jsonBlock);
            this._append(new Block(block.index, block.timestamp, block.blockData, block.previousHash, block.difficulty, block.nonce));
        }
    }

    /**
     * @description 区块入链
     * @param {Block} block
     * @private
     */
    _append(block) {
        this.chain.push(block);
    }

    /**
     * @description 获取链的索引最大值
     * @return {number}
     */
    maxIndex() {
        return this.chain.length;
    }

    /**
     * @description 导出链条数据
     * @returns {Block[]}
     */
    export() {
        return this.chain;
    }

    /**
     * @description 验证链条完整性
     * @returns {boolean}
     */
    verify() {
        const length = this.chain.length;
        for (let i = 1; i < length; i++) {
            /**
             * @type Block
             */
            const previousBlock = this.chain[i - 1];
            /**
             * @type Block
             */
            const currentBlock = this.chain[i];
            if (parseInt(currentBlock.index) !== i) return false;
            if (!currentBlock.verify()) return false;
            if (!(currentBlock.previousHash === previousBlock.hash)) return false;
        }
        return true;
    }
}